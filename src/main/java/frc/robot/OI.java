/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;

/**
 * Add your docs here.
 */
public class OI {
    private Joystick throttle;
    private Joystick steering;
    private XboxController mechControl;

    public OI(){
        this.throttle = new Joystick(0);
        this.steering = new Joystick(1);
        this.mechControl = new XboxController(2);

    }//end constructor

    public double getThrottle(boolean isInverted){
        double throttleVal;
        if (isInverted){
            throttleVal = -throttle.getY();
        }//end if
        else{
            throttleVal = throttle.getY();
        }//end else
        return throttleVal;
    }//end getThrottle

    public double getSteering(boolean isInverted){
        double wheelVal;
        if (isInverted){
            wheelVal = -steering.getY();
        }//end if
        else{
            wheelVal = steering.getY();
        }//end else
        return wheelVal;
    }//end getSteering

    public boolean[] getCoButtons(){
        boolean[] buttonValues = new boolean[14];
        buttonValues[0] = mechControl.getAButton();
        buttonValues[1] = mechControl.getBButton();
        buttonValues[2] = mechControl.getXButton();
        buttonValues[3] = mechControl.getYButton();
        return buttonValues;

    }//end getCoButtons

    public double[] getCoSticks(){
        double[] stickValues = new double[6];
        stickValues[0] = mechControl.getX(Hand.kLeft);
        stickValues[1] = mechControl.getY(Hand.kLeft);
        stickValues[2] = mechControl.getX(Hand.kRight);
        stickValues[3] = mechControl.getY(Hand.kRight);
        stickValues[4] = mechControl.getTriggerAxis(Hand.kLeft);
        stickValues[5] = mechControl.getTriggerAxis(Hand.kRight);
        return stickValues;

    }//end getCoSticks
}//end class

