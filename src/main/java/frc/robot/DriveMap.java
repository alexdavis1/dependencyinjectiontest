/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.SpeedControllerGroup;

/**
 * Add your docs here.
 */
public class DriveMap {
    private SpeedControllerGroup leftDrive;
    private Encoder leftEncoder;

    private SpeedControllerGroup rightDrive;
    private Encoder rightEncoder;

    private Solenoid shifterSolenoid;

    public DriveMap(){
        var leftTopMotor = new WPI_TalonSRX(0);
    }//end constructor
    

}//end class
